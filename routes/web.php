<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [\App\Http\Controllers\PageController::class, 'index']);
Route::get('/about', [\App\Http\Controllers\PageController::class, 'about']);
// user
Route::name('user.')->group(function(){

    Route::view('/dashboard', 'dashboard')->middleware('auth')->name('dashboard');

    Route::get('/login', function (){
        if (Auth::check()) {
            return redirect(route('user.dashboard')); }
        return view('login');
    })->name('login');

    Route::post('/login', [\App\Http\Controllers\LoginController::class, 'login']);

    Route::get('/logout', function (){
        Auth::logout();
        return redirect('/');
    })->name('logout');

    Route::get('/registration', function (){
        if (Auth::check()) {
            return redirect(route('user.dashboard'));
        }
        return view('registration');
    })->name('registration');

    Route::post('/registration',[\App\Http\Controllers\RegisterController::class, 'save']);
});

// топики уроки
Route::name('topic.')->group(function (){
    Route::get('/topic', [\App\Http\Controllers\TopicController::class, 'index'])->name('index');
    Route::view('/topic/create', 'topic.create')->middleware('auth')->name('create');
    Route::post('/topic/store',[\App\Http\Controllers\TopicController::class, 'store'])->name('store');
    Route::get('/topic/edit/{id}', [\App\Http\Controllers\TopicController::class, 'edit'])->name('edit');
    Route::post('/topic/update', [\App\Http\Controllers\TopicController::class, 'update'])->name('update');
    Route::get('/topic/{id}/learners',[\App\Http\Controllers\TopicController::class, 'learnersedit'])->name('learnersedit');
    Route::get('/topic/go/{key}/{topicId?}:{unitId?}',[\App\Http\Controllers\TopicController::class, 'go'])->name('go');
   // Route::any('/test', [\App\Http\Controllers\TopicController::class, 'test'])->name('test');
});

//юниты
Route::name('unit.')->group(function (){
    Route::post('/unit/store', [\App\Http\Controllers\UnitController::class, 'store'])->name('store');
    Route::get('/unit/{id}', [\App\Http\Controllers\UnitController::class, 'show'])->name('show');
    Route::delete('/unit/delete', [\App\Http\Controllers\UnitController::class, 'destroy'])->name('destroy');
    Route::post('unit/update', [\App\Http\Controllers\UnitController::class, 'update'])->name('update');
});

//Ученики
Route::name('learner.')->group(function (){
    Route::get('/learners', [\App\Http\Controllers\LearnerController::class, 'index'])->name('index');
    Route::get('/learners/info', [\App\Http\Controllers\LearnerController::class, 'info'])->name('info');
    Route::post('/learner/store', [\App\Http\Controllers\LearnerController::class, 'store'])->name('store');
    Route::delete('/learner/delete', [\App\Http\Controllers\LearnerController::class, 'destroy'])->name('destroy');
    Route::post('/learner/topicstore', [\App\Http\Controllers\LearnerController::class, 'topicstore'])->name('topicstore');
});
