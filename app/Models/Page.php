<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Page extends Model
{
    use HasFactory;
    protected $table = 'pages';
    protected $fillable = [
        'name',
        'text',
    ];

    public static function getPageByName($namePage)
    {
       // return $page = DB::select('select text from pages where name = :name', ['name' => $namePage]);
       return DB::table('pages')->where('name', $namePage)->first();
    }
}
