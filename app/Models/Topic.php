<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Topic extends Model
{
    use SoftDeletes;

    protected $table = 'topics';

    protected $fillable = [
        'id', // Идентификатор топика(урока)
        'name', // Название
        'is_active', // Статус топика активный\не активный
        'status' // 1 - В работе, 2 - Запущен, 0 - в архиве

    ];

    public function units(){
        return $this->hasMany(Unit::class);
    }

    public function consolidates(){
        return $this->hasMany(Consolidate::class);
    }
}
