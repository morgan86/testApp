<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Consolidate extends Model
{
    use HasFactory;
    protected $table = 'learner_topic';

    protected $fillable = [
        'topic_id',
        'learner_id',
        'url',
        'unit_id',
        'status',
        'result'
    ];

    public function topic(){
        return $this->belongsTo(Topic::class);
    }

    public function learner(){
        return $this->belongsTo(Learner::class);
    }

    public function units()
    {
        return $this->belongsToMany(Unit::class);
    }
}
