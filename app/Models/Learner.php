<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Learner extends Model
{
    use HasFactory;
    protected $table = 'learners';
    protected $fillable = [
        'id',
        'email'
    ];

    public function consolidates(){
        return $this->hasMany(Consolidate::class);
    }
    public function topics(){
        return $this->hasMany(Topic::class);
    }


}
