<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Unit extends Model
{
    use HasFactory;

    protected $table = 'units';

    protected $fillable = [
        'id', // Идентификатор блока
        'topic_id',
        'name', // Название
        'type', // Тип блока
        'start', // логическое поле является ли блок стартовым [да\нет]
        'param', //  в формате json
        'exits' //


    ];

    public function topic(){
        return $this->belongsTo(Topic::class);
    }

    public function consolidates()
    {
        return $this->belongsToMany(Consolidate::class);
    }
}
