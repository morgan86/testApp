<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
   public function login(Request $request){
       if(Auth::check()){
           return redirect()->intended(route('user.dashboard'));
       }
       $fields = $request->only(['email', 'password']);

       if(Auth::attempt($fields)){
           return redirect()->intended(route('user.dashboard'));
       }

       return redirect(route('user.login'))->withErrors([
           'error' => 'Ошибка авторизации'
       ]);
   }
}
