<?php

namespace App\Http\Controllers;

use App\Models\Consolidate;
use App\Models\Topic;
use App\Models\Unit;
use Illuminate\Http\Request;

class TopicController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Topic $model)
    {
        $allTopics=Topic::all();
        //var_dump($allTopics);
        foreach ($allTopics as $topic) {
            $units=Topic::find($topic['id'])->units;
            $topic['units'] =  json_encode($units);
            $topic['unit_count'] = count($units);
        }

        return view('topic.index', ['topics' => $allTopics]);
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Topic $model)
    {
        $model = $model->create($request->all());
        return redirect('topic/edit/'.$model->id);
        //print_r($model->id);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Topic  $topic
     * @return \Illuminate\Http\Response
     */
    public function show(Topic $topic)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Topic  $topic
     * @return \Illuminate\Http\Response
     */
    public function go(Topic $topic,$key,$topicId=0,$unitId=0){
        $error='';
        $currentUnit[0]='';
        $consolidate = Consolidate::where('url', $key)->first();

        if (!isset($consolidate)){
            $error='Ссылка не действительна!';
        } else {
             //Если не пришли значения юнит ID  и топик ID и поле сохранения пройденного юнита = 0
            // то подразумеваем что урок запущен первый раз и начинаем со стартового блока
            if ($topicId==0 && $unitId==0 && $consolidate['unit_id'] == '0') {
                //получаем стартовый юнит нужного топика
                Consolidate::where('url', $key)->update(['status'=>1]);
                $currentUnit = Unit::where([
                    ['topic_id','=',$consolidate['topic_id']],
                    ['start','=','1']])->get();
            } elseif ($topicId==-1) {
                //если приходит -1 то завершаем топик
                Consolidate::where('url',$key)->update(['unit_id'=>$topicId]);
                Consolidate::where('url', $key)->update(['status'=>2]);
                $error='Топик завершен!';

            } elseif ($topicId=='0') {

                Consolidate::where('url',$key)->update(['unit_id'=>$topicId]);
                $error='0';
            } elseif ($topicId!=0 && $unitId!=0) {

                $currentUnit = Unit::where([
                    ['topic_id','=',$topicId],
                    ['id','=',$unitId]])->get();

                   if(count($currentUnit)>0) {
                       Consolidate::where('url',$key)->update(['unit_id'=>$unitId]);
                     // dd($currentUnit[0]->id);
                   } else {
                       $error='Блок или топик не найден! Возможно он был удален!';
                   }


            } elseif ($consolidate['unit_id'] == '-1'){
                $error='0';
                Consolidate::where('url', $key)->update(['status'=>0]);
            } elseif ($consolidate['unit_id'] != '-1' && $consolidate['unit_id'] != '0' && $topicId=0 && $unitId=0){
                //не первый и не последний юнит
                $currentUnit = Unit::where([
                    ['topic_id','=',$consolidate['topic_id']],
                    ['id','=',$consolidate['unit_id']]])->get();

            }


            }



        if (count($currentUnit)==0) {$error = "Возникла неизвестная ошибка ! Попробуйте еще раз!";}
        return view('topic.go', ['unit' => $currentUnit[0],'key'=>$key, 'error'=>$error]);
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Topic  $topic
     * @return \Illuminate\Http\Response
     */
    public function edit($id, Topic $topic)
    {


        $topic=$topic->find($id);
        $topic['token']=csrf_token();
        return view('topic.edit', ['topic' => $topic]);
    }

    public function learnersedit($id,Topic $topic){





        $topic=$topic->find($id);
        $topic['units']=$topic->find($id)->units;


        return view('topic.learnersedit', ['topic' => $topic]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Topic  $topic
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Topic $model)
    {
        $dataResult = $request->all();

        $model = $model ->find($dataResult['id']);
        $model->name = $dataResult['name'];
        $model->status = $dataResult['status'];
        if(isset($dataResult['is_active'])){
            $model->is_active = 1;
        } else {
            $model->is_active = 0;
        }


        $model->update();
        return redirect('topic');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Topic  $topic
     * @return \Illuminate\Http\Response
     */
    public function destroy(Topic $topic)
    {
        //
    }
}
