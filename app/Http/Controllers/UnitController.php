<?php

namespace App\Http\Controllers;

use App\Models\Unit;
use Illuminate\Http\Request;
use function Psr\Log\error;

class UnitController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Unit $unit)
    {
       $dataForBase=$request->all();
    //   print_r($dataForBase);
    //  die();
       // $dataForBase = json_decode($dataForBase['data']);

           $unit=Unit::create ([
                "topic_id"=>$dataForBase['data']['topic_id'],
                "name"=>$dataForBase['data']['name'],
                "type"=>$dataForBase['data']['type'],
                "param"=>json_encode($dataForBase['data']['param']),
                "exits"=>json_encode($dataForBase['data']['exits'])
            ]);

         return response()->json($unit);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Unit  $unit
     * @return \Illuminate\Http\Response
     */
    public function show(Unit $unit, $topicID)
    {
        $units = Unit::where('topic_id', $topicID)->get();
        return response()->json($units);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Unit  $unit
     * @return \Illuminate\Http\Response
     */
    public function edit(Unit $unit)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Unit  $unit
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Unit $unit)
    {
        $dataResult = $request->all('data');

        $unit = $unit ->find($dataResult['data']['id']);

        $unit->name = $dataResult['data']['name'];
        $unit->type = $dataResult['data']['type'];
        $unit->start = $dataResult['data']['start'];
        $unit->param = json_encode($dataResult['data']['param']);
        $unit->exits = json_encode($dataResult['data']['exits']);
        $unit->topic_id = $dataResult['data']['topic_id'];

        $unit->update();
        return $unit;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Unit  $unit
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, Unit $unit)
    {

        $data = $request->all();
        return Unit::destroy($data['id']);

    }
}
