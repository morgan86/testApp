<?php

namespace App\Http\Controllers;


use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
class RegisterController extends Controller
{
   public function save (Request $request){
       if(Auth::check()){
           return redirect(route('user.dashboard'));
       }

       $validateFields = $request->validate([
           'email' => 'required|email'
       ]);
       if(User::where('email', $validateFields['email'])->exists()){
           return redirect(route('user.registration'))->withErrors([
               'error' => 'Такой Email уже есть в БД!'
           ]);
       }

       $validateFields['password'] = substr(md5(time()), 10, 6); // Простая генерация пароля:берем последние 6 символов мд5 хеша текущего времени

       $user = User::create($validateFields);
       Log::info('Регистрация нового пользователя - email: '.$validateFields['email'].' Пароль: '.$validateFields['password']);
       if($user) {
           Auth::login($user);
          // return redirect(route(user.dashboard));
       }

       return redirect(route('user.login'))->withErrors([
           'error' => 'Произошла ошибка ! Попробуйте еще раз, если ошибка повторится сообщите администрации сайта. '
       ]);
   }
}
