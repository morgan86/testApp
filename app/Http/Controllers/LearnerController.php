<?php

namespace App\Http\Controllers;

use App\Models\Consolidate;
use App\Models\Learner;
use App\Models\Topic;
use Illuminate\Http\Request;

class LearnerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request){

        $allLearners=Learner::all();

        foreach ($allLearners as $learner) {
            $con=Learner::find($learner['id'])->consolidates;
            if ($con) {
                $temp=[];
                foreach ($con as $value){

                   // print_r("<br>Ученик с ID:".$learner['id'].' содержит состоит в топиках: '.$value['topic_id']);
                   $topicName = Topic::find($value['topic_id']);

                   $temp[]=$value['topic_id'].":".$topicName['name'];
                }
                $learner['topics']=$temp;
            }
            //$learner['topics'] =  json_encode($con);

        }


        $topics= Topic::where('status',2)->get();
        if ($request->ajax()) {

            return response()->json($allLearners);
          } else {
            return view('learner.index', ['topics' =>$topics]);
        }

    }

    public function info(){
        $learners=Learner::all();
        $resultArr = [];
        $i=0;
        foreach ($learners as $learner) {
            $consolidate=Learner::find($learner['id'])->consolidates;
            $resultArr[$i]=['id'=>$learner->id, 'email'=>$learner->email, 'allTopicsCount'=>count($consolidate)];

                foreach ($consolidate as $value){
                    $units=Topic::find($value['topic_id'])->units;
                    $topicName = Topic::find($value['topic_id']);

                    foreach ($units as $unit) {
                        $tmp2[]=['id'=>$unit->id,'name'=>$unit->name, 'type'=>$unit->type, 'start'=>$unit->start, 'param'=>$unit->param, 'exits'=>$unit->exits, 'topic_id'=>$unit->topic_id];
                    }

                    $tmp=['topic_id'=>$value['topic_id'], 'url'=>$value['url'], 'status'=>$value['status'], 'result'=>$value['result'], 'unit_id'=>$value['unit_id'], 'topicName'=> $topicName['name'], 'units'=>$tmp2];
                    $resultArr[$i]['consolidate'][]=$tmp;
                }
                $i++;
        }

//dd($resultArr);
        return view('learner.info', ['learners' =>$resultArr]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Learner $learner)
    {
        $dataResult = $request->all();
        $emailList = array_diff($dataResult['email'], array(''));
        $resultStore = [];

        foreach ($emailList as $email) {
            if (Learner::where('email', $email)->exists()){
                $resultStore[]=$email." - уже есть в в базе ";
            } else if(Learner::create(['email'=>$email])) {
                $resultStore[]=$email." - Успешно записан в БД";
            } else {
                $resultStore[]=$email." - Ошибка добавления в БД";
            }
        }
        return response()->json($resultStore);
    }

    public function topicstore(Request $request, Learner $learner)
    {
        $dataResult = $request->all();
        $resultStore = [];


        foreach ($dataResult['learnersID'] as $item) {
            $lernerIsset = Consolidate::where('learner_id', $item)->first();
            if(isset($lernerIsset['topic_id']) && $lernerIsset['topic_id']==$dataResult['topicID']) {
                $resultStore[]='Ученик с ID:'.$item.' уже есть в этом топике';
            } else {
                $res=Consolidate::create ([
                    "topic_id"=>$dataResult['topicID'],
                    "learner_id"=>$item,
                    "url"=>md5($item.'salt'.time()),
                    "unit_id"=>0,
                    "status"=>0,
                    "result"=>'[]'
                ]);
                if ($res) {
                    $resultStore[]='Ученик с ID:'.$item.' успешно добавлен к топику';
                } else {
                    $resultStore[]='Ошибка записи в БД ! Попробуйте еще разок!';
                }
            }

        }







        return response()->json($resultStore);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Learner  $learner
     * @return \Illuminate\Http\Response
     */
    public function show(Learner $learner)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Learner  $learner
     * @return \Illuminate\Http\Response
     */
    public function edit(Learner $learner)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Learner  $learner
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Learner $learner)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Learner  $learner
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, Learner $learner)
    {
        $resultStore=[];
        $data = $request->all();
        foreach ($data['ids'] as $id) {
           if(Learner::destroy($id)) {
               $resultStore[]="Ученик с ID:".$id." Успешно удален ! ";
           } else {
               $resultStore[]="Ошибка удаления! ";
           }

        }

        return response()->json($resultStore);

    }
}
