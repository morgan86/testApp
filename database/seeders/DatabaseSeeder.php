<?php

namespace Database\Seeders;

use Database\Factories\PageFactory;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\App;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {

        \App\Models\Page::factory(1)->create(['name'=>'main_page']);
        \App\Models\Page::factory(1)->create(['name'=>'about_page']);
    }
}
