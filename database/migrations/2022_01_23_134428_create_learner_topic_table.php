<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLearnerTopicTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('learner_topic', function (Blueprint $table) {
            $table->foreignId('topic_id')->constrained()->onDelete('cascade');
            $table->foreignId('learner_id')->constrained()->onDelete('cascade');
            $table->string('url');
            $table->integer('unit_id');
            $table->tinyInteger('status');
            $table->json('result')->default('');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('learner_topic');
    }
}
