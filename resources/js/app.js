require('./bootstrap');
import { createApp } from 'vue';
import Topic from './components/Unit';
import Learners from './components/LearnersEdit';
import Quiz from './components/Quiz';
const app = createApp({})
app.component('Topic', Topic)
app.component('Learners', Learners)
app.component('Quiz', Quiz)
app.mount('#app')
