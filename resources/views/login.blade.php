@include("layouts.header")
<div class="container">
    <div class="row justify-content-md-center">
        <div class="col-md-auto">
            <h3 class="p-3">Вход</h3>
            <form method="POST" action="{{ route('user.login') }}">
                @csrf
                <div class="input-group mb-3">
                    <span class="input-group-text" id="basic-addon1">Email:</span>
                    <input type="email" name="email" id="email" class="form-control"  aria-describedby="basic-addon1">
                </div>

                <div class="input-group mb-3">
                    <span class="input-group-text" id="basic-addon1">Пароль:</span>
                    <input type="password" name="password" id="password" class="form-control" id="password">
                </div>

                @error('error')
                <div class="alert alert-danger">{{ $message }}</div>
                @enderror

                <button type="submit" class="btn btn-primary" name="sendMe" value="1">Войти</button>
            </form>
        </div>
    </div>
</div>


@include("layouts.footer")
