@include("layouts.header")

<div class="container">
    <div class="row justify-content-md-center">
        <div class="col-md-auto">
            <h3 class="p-3">Регистрация</h3>
            <form method="POST" action="{{ route('user.registration') }}">
                @csrf
                <div class="input-group mb-3">
                    <span class="input-group-text" id="basic-addon1">Email:</span>
                    <input type="email" name="email" id="email" class="form-control"  aria-describedby="basic-addon1">
                </div>
                @error('email')
                <div class="alert alert-danger">{{ $message }}</div>
                @enderror
                <div class="alert alert-dark" role="alert">
                    Пароль для входа придет на указанный email адрес
                </div>


                <button type="submit" class="btn btn-primary" name="sendMe" value="1">Зарегистрироваться</button>
            </form>
        </div>
    </div>
</div>

@include("layouts.footer")
