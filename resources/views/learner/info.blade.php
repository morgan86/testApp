@include("layouts.header")
<div class="container">
    <div class="row row-cols-4">
        <div class="col-12 text-center">
            <h5 class="mb-2 mt-3">Просмотр полной информации об учениках и их действиях</h5>
            <hr>
        </div>
    </div>
    <div class="row row-cols-4">
        <div class="col-12">

            @foreach($learners as $key=>$item)
                <div class="mt-2 accordion" id="accordion">
                    <div class="accordion-item bg-dark">
                        <h2 class="accordion-header" id="flush-heading{{$item['id']}}">
                            <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapse{{$item['id']}}" aria-expanded="false" aria-controls="flush-collapse{{$item['id']}}">
                                <span class="mx-1">ID:<strong class="mx-2">{{$item['id']}}</strong></span>
                                <span class="mx-3">Email:<strong class="mx-2">{{$item['email']}}</strong></span>
                                <span class="mx-3">Топиков всего:<strong class="mx-2">{{$item['allTopicsCount']}}</strong></span>
                            </button>
                        </h2>
                        <div id="flush-collapse{{$item['id']}}" class="accordion-collapse collapse bg-dark" aria-labelledby="flush-heading{{$item['id']}}" data-bs-parent="#accordion">
                            @if(isset($item['consolidate']))
                                @foreach($item['consolidate'] as $value)
                                    <div class="mx-3 mt-3 mb-3 card">
                                        <div class="card-header">
                                            #{{$value['topic_id']}}<span class="mx-2">{{$value['topicName']}}</span>
                                            @if($value['status']==0)
                                                <span class="bg-secondary mx-3">[не запущен]</span>
                                            @elseif($value['status']==1)
                                                <span class="bg-info mx-3">[в учебе]</span>
                                            @else
                                                <span class="bg-success mx-3">[пройден]</span>
                                            @endif
                                            <div class="text-end"><strong>Блоков:{{count($value['units'])}} </strong></div>
                                        </div>
                                        <div class="card-body">
                                            @if(isset($value['units']))
                                                @foreach($value['units'] as $key=>$unit)
                                                    <div class="card mt-2">
                                                        <div class="card-header">
                                                            @if($unit['type']=='info')
                                                                Информационный блок (id:{{$unit['id']}})@if($unit['id'] ==$value['unit_id']) <span class="badge bg-warning text-dark">В процессе</span> @endif
                                                                @if($unit['start']==1)
                                                                   <strong class="mx-5">Стартовый</strong>
                                                                @endif
                                                        </div>
                                                        <div class="card-body">
                                                            <h5 class="card-title">{{$unit['name']}}</h5>
                                                            <p class="card-text">
                                                                @if(isset($unit['exits']))
                                                                    Выходы
                                                                @foreach (json_decode($unit['exits']) as $exit)
                                                                    <strong>{{$exit->link}}</strong>
                                                                @endforeach
                                                                @endif
                                                            </p>
                                                        </div>
                                                    </div>
                                                            @elseif($unit['type']=='choice')
                                                               Блок с условием (id:{{$unit['id']}})@if($unit['id'] ==$value['unit_id']) <span class="badge bg-warning text-dark">В процессе</span> @endif
                                                               @if($unit['start']==1)
                                                                  <strong class="mx-5">Стартовый</strong>
                                                             @endif
                                                    </div>
                                        <div class="card-body">
                                            <h5 class="card-title">{{$unit['name']}}</h5>
                                            <p class="card-text">
                                                @if(isset($unit['exits']))
                                                    Выходы
                                                    @foreach (json_decode($unit['exits']) as $exit)
                                                        <strong>{{$exit->link}}</strong>
                                                    @endforeach
                                                @endif

                                            </p>

                                        </div>
                                    </div>

                                                            @else
                                                                Блок с опросом  (id:{{$unit['id']}})@if($unit['id'] ==$value['unit_id']) <span class="badge bg-warning text-dark">В процессе</span> @endif
                                        @if($unit['start']==1)
                                            <strong class="mx-5">Стартовый</strong>
                                        @endif
                        </div>
                        <div class="card-body">
                            <h5 class="card-title">{{$unit['name']}}</h5>
                            <p class="card-text">
                                @if(isset($unit['exits']))
                                    Выходы
                                    @foreach (json_decode($unit['exits']) as $exit)
                                        <strong>{{$exit->link}}</strong>
                                    @endforeach
                                @endif

                            </p>

                        </div>
                    </div>
                                                            @endif





                                                 @endforeach
                                                @else
                                                в этом топике нет юнитов
                                                @endif


                                        </div>
                <hr>
                <p class="mx-5">Персональная ссылка <a href="/topic/go/{{$value['url']}}">{{$value['url']}}</a> </p>
                                    </div>

                                @endforeach
                            @else
                                <div class="accordion-body text-white">
                                   Данному ученику еще не назначен ни один топик!
                                </div>

                            @endif
                        </div>
                    </div>
                </div>
            @endforeach

        </div>
    </div>
</div>



@include("layouts.footer")
