@include("layouts.header")

<div id="app">
    <Learners :topics='@json($topics)'></Learners>
</div>


@include("layouts.footer")
