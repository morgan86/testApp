@include("layouts.header")

<h2>Dashboard</h2>
<h5>Добро пожаловать на сайт - <strong>{{Auth::user()->email }}</strong>. Вы успешно авторизовались! </h5>

@include("layouts.footer")
