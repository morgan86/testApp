@include("layouts.header")
<div class="container">
    <div class="row row-cols-4">
        <div class="col-6">
            <h5 class="mt-3">Страница создания урока</h5>
            <form method="post" action="{{ route('topic.store') }}" enctype="multipart/form-data">
                @csrf
                <label class="mt-5">Укажите название урока</label>
                <input type="text" class="mt-2 form-control -lg" required  name="name">
                <input type="submit" class="mt-5 btn btn-outline-info" value="Создать">
            </form>
        </div>
    </div>
</div>








@include("layouts.footer")
