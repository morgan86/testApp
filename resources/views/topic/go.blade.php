@include("layouts.header")
@if($error=='')
    @if($unit->type == 'info')
        <div class="container">
            <div class="row row-cols-4">
                <div class="col-12 text-center">
                    <h5 class="mb-2 mt-5">Информационный блок</h5>
                    <hr>
                </div>
            </div>
            <div class="row row-cols-4">
                <div class="col-12">
                    <p mx-5><strong>Текст:</strong>
                        {{$unit->name}}
                    </p>
                </div>
            </div>
            <div class="row row-cols-4">
                <div class="col-12 text-center">
                    @foreach (json_decode($unit->exits) as $value)
                        <a href="/topic/go/{{$key}}/{{$value->link}}"><input type="button" class="mt-5 p-3 btn btn-success" value="Ознакомился"></a>
                    @endforeach
                </div>
            </div>
        </div>
    @endif

    @if($unit->type == 'choice')
        <div class="container">
            <div class="row row-cols-4">
                <div class="col-12 text-center">
                    <h5 class="mb-2 mt-5">Юнит с условием</h5>
                    <hr>
                </div>
            </div>
            <div class="row row-cols-4">
                <div class="col-12">
                    <p mx-5><strong>Текст:</strong>
                        {{$unit->name}}
                    </p>
                </div>
            </div>
            <div class="row row-cols-4">
                <div class="col-12 text-center">
                    @foreach (json_decode($unit->exits) as $value)
                        <a href="/topic/go/{{$key}}/{{$value->link}}"><input type="button" class="mt-5 mx-3 p-3 btn btn-success" value="{{$value->text}}"></a>
                    @endforeach
                </div>
            </div>
        </div>
    @endif
    @if($unit->type == 'quiz')
         <div class="container">
            <div class="row row-cols-4">
                <div class="col-12 text-center">
                    <h5 class="mb-2 mt-5">Блок с опросом</h5>
                    <hr>
                </div>
            </div>
            <div class="row row-cols-4">
                <div class="col-12">
                    <p mx-5><strong>Вопрос:</strong>
                        {{$unit->name}}
                    </p>
                </div>
            </div>
            <div class="row row-cols-4">
                <div class="col-12">
                    <p mx-5><strong>Отметьте верные ответы:</strong></p>
                    <div id="app">
                        <Quiz
                            :unit='@json($unit->param)'
                            keyhash='{{$key}}'
                            exits='@json($unit->exits)'
                        ></Quiz>
                    </div>
                </div>
            </div>
        </div>
    @endif


@else
    <div class="container">
        <div class="row row-cols-4">
            <div class="col-12 text-center">
                <hr>
                <h4 class="mb-2 mt-5">
                    @if($error=='0')
                        Топик уже завершен! <a href="/topic/go/{{$key}}/0" ><br>Пройти еще раз</a>
                     @else
                        {{$error}}
                    @endif
                </h4>
                <hr>
            </div>
        </div>
    </div>
@endif
@include("layouts.footer")
