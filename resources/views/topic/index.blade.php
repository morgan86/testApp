@include("layouts.header")

<div class="container">
    <div class="row row-cols-4">
        <div class="col-12 text-center">
            <h5 class="mb-2 mt-3">Уроки </h5>
            <hr>
        </div>
    </div>
    <div class="accordion accordion-flush" id="accordionFlush">

        @foreach ($topics as $item)

            <div class="accordion-item">
                <h2 class="accordion-header" id="flush-heading{{$item->id}}">
                    <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapse{{$item->id}}" aria-expanded="false" aria-controls="flush-collapse{{$item->id}}">
                        <div class="container">
                            <div class="row">
                                <div class="col">
                                    @if($item->is_active === 1)
                                        <span class="material-icons">done_outline</span>
                                    @else
                                        <span class="material-icons">remove_circle</span>
                                    @endif
                                </div>
                                <div class="col">
                                    ID: <strong class="mx-2">{{$item->id}}</strong>
                                </div>
                                <div class="col">
                                    Блоков:<strong class="mx-2">{{$item->unit_count}}</strong>
                                </div>
                                <div class="col">
                                    <strong class="mx-2">{{$item->name}}</strong>
                                </div>
                                <div class="col">
                                    Дата создания: <strong class="mx-2">{{$item->created_at}}</strong>
                                </div>
                            </div>
                        </div>
                    </button>
                </h2>
                <div id="flush-collapse{{$item->id}}" class="accordion-collapse collapse" aria-labelledby="flush-heading{{$item->id}}" data-bs-parent="#accordionFlush">
                    <div class="accordion-body">
                        @foreach (json_decode($item->units) as $unit)
                            @if($unit->type == 'info')
                                <p><strong class="mx-2">ID:</strong> {{ $unit->id }}
                                    <strong class="mx-2">Тип блока:</strong> Информационный
                                    <strong class="mx-2">Текст:</strong>{{ $unit->name }}
                                </p>
                                <hr>
                            @endif
                            @if($unit->type == 'choice')
                                    <p><strong class="mx-2">ID:</strong> {{ $unit->id }}
                                        <strong class="mx-2">Тип блока:</strong> Условие
                                        <strong class="mx-2">Текст:</strong>{{ $unit->name }}
                                    </p>
                                <hr>
                            @endif
                            @if($unit->type == 'quiz')

                                    <p> <strong class="mx-2">ID:</strong>  {{ $unit->id }}
                                        <strong class="mx-2">Тип блока:</strong> Блок с вопросами
                                        <strong class="mx-2">Вопрос: </strong>{{ $unit->name }}

                                    </p>
                                    <p>


                                        @foreach (json_decode($unit->param)  as $key=>$answer)

                                            {{$key}}
                                            @if($key=='allAnswer')
                                                <strong class="mx-2">Варианты ответов: </strong>@json($answer[0])
                                            @endif

                                        @endforeach


                                    </p>




                                    <hr>
                            @endif


                        @endforeach
<a href="/topic/edit/{{$item->id}}">Редактировать урок</a>
                    </div>
                </div>
            </div>



        @endforeach

    </div>
</div>

@include("layouts.footer")
