@include("layouts.header")



<div id="app">
    <div class="relative flex items-top justify-center min-h-screen bg-gray-100 sm:items-center py-4 sm:pt-0">
        <Topic :topic='@json($topic)'></Topic>
    </div>
</div>

@include("layouts.footer")
